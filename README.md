[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687073.svg)](https://doi.org/10.5281/zenodo.4687073)

# Estimated share of gross floor area per construction period - 2000 to 2014


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation


### Limitations of the dataset
For the use and the estimation of the reliability of the data it is important to keep in mind that the data maps build on a statistical approach and do not take site specific or local conditions into account.
For the residential heated gross floor area, statistical data are available for most countries on the level of NUTS3. Again, manually performed data quality checks indicate that results are plausible on the hectare level of most regions. However, as of now, we do not factor in the fact, that the heat area per inhabitant often decrease with an increasing population density. For  NUTS3  regions  with  a  strong  urban  versus  rural  area  gradient,  this  might  lead  to  overestimation of the heated residential gross floor area in urban areas. Regarding the heated gross floor area of non‐residential buildings, data sources are even uncertain on the NUTS0 level. Data quality checks indicate that the sum of residential and non‐ residential heated gross floor area are plausible as well as the ratio between residential and non‐residential gross floor area,  even  though  the  later  indicator  might  not  hold  for  grid  cells which contain only  few  buildings.


### References


## How to cite
Andreas Mueller, "Estimated share of gross floor area per construction period2", Technische Universität Wien, 2019. https://gitlab.com/hotmaps/potential/share_gfa_per_construction_period

## Authors
Andreas Mueller <sup>*</sup>

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/), Institute of Energy Systems and Electrical Drives, Gusshausstrasse 27-29/370, 1040 Wien


## License
Copyright © 2016-2019: Andreas Mueller
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

